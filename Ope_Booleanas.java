
package ope_booleanas;

import java.util.Scanner;

/**
 *
 * @author Snow
 */
public class Ope_Booleanas {

    
    public String and(String bin1, String bin2)
    {
        String cadena="";
              if(bin1.length()==bin2.length() && (bin2.length()==8||bin2.length()==4))
              {
     for(int i=0;i<bin1.length();i++)
{
    if(bin1.charAt(i)=='1' && bin2.charAt(i)=='0' || bin1.charAt(i)=='0' && bin2.charAt(i)=='1' || bin1.charAt(i)=='0' && bin2.charAt(i)=='0')
    {
        cadena+="0";
    }
    else  
       cadena+="1";
 }        return cadena;
        }
        else return "";
   }
    
    public String or(String bin1, String bin2)
    {
         String cadena="";
       // if(bin1==1 && bin2==0 || bin1==0 && bin2==1|| bin1==1 && bin2==1)
         if(bin1.length()==bin2.length() && (bin2.length()==8||bin2.length()==4))
         {
     for(int i=0;i<bin1.length();i++)
{
    if(bin1.charAt(i)=='1' && bin2.charAt(i)=='0' || bin1.charAt(i)=='0' && bin2.charAt(i)=='1' || bin1.charAt(i)=='1' && bin2.charAt(i)=='1')
    {
        cadena+="1";
    }
    else  
       cadena+="0";
 }        return cadena;
        }
        else return "";
         
    }
    
    public String inversor(String cadena)
 {
     StringBuilder inver = new StringBuilder(); 
inver = new StringBuilder(); 


for(int i=0;i<cadena.length();i++)
{
    if(cadena.charAt(i)=='0')
    {
        inver.append("1");
    }
    else
    
        if(cadena.charAt(i)=='1')
       { inver.append("0");
        }
 }
        return String.valueOf(inver);
 }
  
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        String bite1, bite2;
        System.out.print("Ingresa los BYTE's");
        System.out.print("\nBYTE1:  ");
        bite1 = leer.next();
        
        System.out.print("BYTE2:  ");
        bite2 = leer.next();
       Ope_Booleanas com=new Ope_Booleanas();
       
       com.leer(bite1, bite2);
       System.out.println("");
       
      
    }

    private void leer(String bite1, String bite2) 
    {
        if(bite1.length()==bite2.length() )
        {
        System.out.println("* AND:  "+and(bite1,bite2));
        
        System.out.println("* OR  : "+or(bite1,bite2));
        System.out.println("NOT:");
        System.out.println("BYTE1:  "+inversor(bite1));
        System.out.println("BYTE2:  "+inversor(bite2));
       System.out.println("\n");
        //¬X ∨ Y
        System.out.println("Ejemplo1: "+or(inversor(bite1),bite2));
        
        //X ∧ ¬Y
        System.out.println("Ejemplo2: "+and(bite1,inversor(bite2)));
        
        //(Y ∧ S) ∨ (X ∧ ¬S)
        //String s="00001111";
        System.out.println("Ejemplo3: "+or((and("00110011","00001111")),(and("01010101",(inversor("00001111")) ))));
       
        }
        else
            System.out.println("Debe ingresar el mismo numero de \ndigitos para bin1 y bin2");
    
    }
}
